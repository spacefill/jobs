# Linter configuration used by SpaceFill

This configuration was created iteratively by the tech team [based on the CTO's historical preference](https://github.com/stephane-klein/personnal-notebook/blob/master/013-javascript-coding-styles-eslint-mes-pr%C3%A9f%C3%A9rences.md).

## ESLint

SpaceFill uses [ESLint](https://github.com/eslint/eslint) with the following plugins:

- [`eslint-config-standard`](https://github.com/standard/eslint-config-standard)
- [`eslint-plugin-graphql`](https://github.com/apollographql/eslint-plugin-graphql)
- [`eslint-plugin-import`](https://github.com/benmosher/eslint-plugin-import)
- [`eslint-plugin-jest`](https://github.com/jest-community/eslint-plugin-jest)
- [`eslint-plugin-node`](https://github.com/mysticatea/eslint-plugin-node)
- [`eslint-plugin-promise`](https://github.com/xjamundx/eslint-plugin-promise)
- [`eslint-plugin-react`](https://github.com/yannickcr/eslint-plugin-react)
- [`eslint-plugin-react-hooks`](https://github.com/facebook/react/tree/master/packages/eslint-plugin-react-hooks)
- [`eslint-plugin-standard`](https://github.com/standard/eslint-config-standard)
- [`eslint-plugin-unicorn`](https://github.com/sindresorhus/eslint-plugin-unicorn)

and this configuration: [`./.eslintrc.json`](./.eslintrc.json).

## Stylelint

SpaceFill uses [stylelint](https://github.com/stylelint/stylelint) with these packages:

- [`stylelint-config-recommended`](https://github.com/stylelint/stylelint-config-recommended)
- [`stylelint-config-styled-components`](https://github.com/styled-components/stylelint-config-styled-components)
- [`stylelint-processor-styled-components`](https://github.com/styled-components/stylelint-processor-styled-components)

and this configuration: [`./.stylelintrc`](./.stylelintrc)